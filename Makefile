-include Makefile.conf

CC := $(CROSS_COMPILE)gcc
AR := $(CROSS_COMPILE)ar

CPPFLAGS := -O2 -ggdb -Wall -Wextra -Wno-unused-parameter
LDFLAGS := -Wl,--no-undefined -Wl,--as-needed

PLATFORM := $(shell $(CC) -dumpmachine | cut -f 3 -d -)

ifeq ($(PLATFORM),mingw32)
  ext := dll
else
  ext := so
endif

prefix := /usr
libdir := $(prefix)/lib
bindir := $(prefix)/bin
name := foobar

srcdir ?= $(dir $(firstword $(MAKEFILE_LIST)))

vpath %.c $(srcdir)

global_deps = $(srcdir)/Makefile Makefile.conf

all:

libbar := libbar.$(ext)
$(libbar): bar.o
$(libbar): LDLIBS =

libfoo := libfoo.$(ext)
$(libfoo): foo.o | $(libbar)
$(libfoo): INCLUDES = -I$(srcdir)
$(libfoo): LDLIBS = -L. -lbar

bazinator: bazinator.o | $(libfoo)
bazinator: INCLUDES = -I$(srcdir)
bazinator: LDLIBS = -L. -lfoo

all: bazinator

Makefile.conf:
	@touch $@

%.so: ext_CFLAGS = -fPIC

%.so %.dll:
	$(QUIET_LINK)$(CC) $(LDFLAGS) -shared $^ $(LDLIBS) -Wl,--enable-new-dtags -Wl,-rpath,$(libdir) -o $@

check: bazinator
ifeq ($(PLATFORM),mingw32)
	bazinator
else
	@LD_LIBRARY_PATH=. ./bazinator
endif

libfoo.pc: $(srcdir)/libfoo.pc.in
	@sed -e 's#@prefix@#$(prefix)#g' \
		-e 's#@version@#$(version)#g' \
		-e 's#@libdir@#$(libdir)#g' $< > $@

# pretty print
ifndef V
QUIET_CC    = @echo '   CC         '$@;
QUIET_LINK  = @echo '   LINK       '$@;
QUIET_CLEAN = @echo '   CLEAN      '$@;
endif

%.o: %.c $(global_deps)
	$(QUIET_CC)$(CC) $(CPPFLAGS) $(CFLAGS) $(ext_CFLAGS) $(INCLUDES) -MMD -MF $(@:%.o=.%.d) -c $< -o $@

%: %.o
	$(QUIET_LINK)$(CC) $(LDFLAGS) $^ $(LDLIBS) -Wl,-rpath-link=. -o $@

%.a:
	$(QUIET_LINK)$(AR) rcs $@ $^

.PHONY: clean

clean:
	$(QUIET_CLEAN)$(RM) -v bazinator *.$(ext) *.o *.d

D = $(DESTDIR)

install: all libfoo.pc
	install -m 755 -D $(libfoo) $(D)$(libdir)/$(libfoo)
	install -m 755 -D $(libbar) $(D)$(libdir)/$(libbar)
	install -m 755 -D bazinator $(D)$(bindir)/bazinator
	install -m 644 -D libfoo.pc $(D)$(libdir)/pkgconfig/libfoo.pc

uninstall:
	$(RM) $(D)$(libdir)/$(libfoo)
	$(RM) $(D)$(libdir)/$(libbar)
	$(RM) $(D)$(bindir)/bazinator

.files:
	@test -d .git && (git ls-files && echo .files) > .files || true

.PHONY: .files

dist: .files
	@tar -cvzf $(name).tar.gz --transform='s#^#$(name)/#' -T .files

distcheck: dist
	tar -xf $(name).tar.gz
	cd $(name); make check; cd ..
	rm -rf $(name)

-include .*.d
